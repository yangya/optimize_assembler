from Bio import SeqIO
import os,sys

"""
take a folder of .good files from output of pick_blat_hits.py
output total gene coverage for each input .good file
"""

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: sum_blat_gene_coverage.py DIR"
		sys.exit(0)

	DIR = sys.argv[1]
	outfile = open(DIR+"/gene_covarage","w")
	header = "file_name\ttotal_coverage(bp)\ttotal_coverage(no._of_genes)\t"
	header += "#assembled_to_>=80%\tredundancy\n"
	outfile.write(header)
	
	for goodhits_file in os.listdir(DIR):
		if goodhits_file[-5:] == ".good":
			infile = open(DIR+"/"+goodhits_file,"rU")
			print "Reading "+goodhits_file
			query_list = []
			Tcov_dict = {} #key is gene name, value is [max T cov,max perc T cov]
			
			for line in infile:
				if len(line) < 3: continue #ignore empty lines
				spls = line.strip().split("\t")
				T,Tcov_perc = spls[13],float(spls[22])
				if T not in Tcov_dict:
					Tcov_dict[T] = [0,0.0]
				if Tcov_perc > Tcov_dict[T][1]:
					Tcov_dict[T][0] = abs(float(spls[16])-float(spls[15]))
					Tcov_dict[T][1] = Tcov_perc
					#use the larger coverage; do not combine coverages
				num_genes += 1
			infile.close()
			
			num_genes = 0 #total number of genes
			total_cov = 0
			gene_80_count = 0 #number of genes assembled to >= 80% length
			for gene in Tcov_dict:
				total_cov += Tcov_dict[gene][0]
				if Tcov_dict[gene][1] >= 0.8: gene_80_count += 1
			gene_covered = len(Tcov_dict)
			redundancy = float(num_genes) / gene_covered
			out = goodhits_file+"\t"+str(total_cov)+"\t"+str(gene_covered)+"\t"
			out += str(gene_80_count)+"\t"+str(redundancy)+"\n"
			outfile.write(out)
			
	outfile.close()

