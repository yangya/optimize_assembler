import sys,os
from Bio import SeqIO

###########
#original transcripts.gtf file is imported into LibreOffice
#input options: tab and semicolon
#output options: save as .csv file, without text delimiter, and separated by tab
#remove all lines that are exon or with 0 coverage
#remove unwanted collums so that:
#col 0: contig number
#col 1: start
#col 2: end
#col 3: relative expression (1-1000)
#col 4: gene_id (remove gene_id and "")
#col 5: transcript_id (remove transcript_id and "")
#col 6: FPKM (remove FPKM and "")
#col 7: cov (remove cov and "")
#col 8: full_read_support (remove full_read_support and "")
#finally, sort entire spreadsheet in assending order with col A and then col B
# get a list of transcript id and coverage:
# awk -F "\t" '{print $8,$6}' '/home/yayang/model/Ricinus/cufflinks_exon_annoted-only_out/expressed_transcripts.csv' | sort -n > expression
###########

if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: calculate_quintile_from_cufflinks_output.py expression num_lines"
		sys.exit()
	
	count = 1.0
	quintile_bin = 1
	num_genes = float(sys.argv[2])
	infile = open(sys.argv[1],"rU")
	outfile = open("./quintile","w")
	for line in infile:
		if len(line) < 3: continue
		line = line.strip().split(' ')
		outfile.write(line[1]+"_"+line[0]+" "+str(quintile_bin)+"\n")
		count += 1
		if count > num_genes/20 * quintile_bin:
			quintile_bin += 1
	infile.close()
	outfile.close()


