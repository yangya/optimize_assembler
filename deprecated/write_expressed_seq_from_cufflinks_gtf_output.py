import sys,os
from Bio import SeqIO

###########
#original transcripts.gtf file is imported into LibreOffice
#input options: tab and semicolon
#output options: save expressed_transcripts.gtf, without text delimiter, and separated by tab
#remove all lines that are exon or with 0 coverage
#remove unwanted collums so that:
#col 0: contig number
#col 1: start
#col 2: end
#col 3: relative expression (1-1000)
#col 4: gene_id (remove gene_id and "")
#col 5: transcript_id (remove transcript_id and "")
#col 6: FPKM (remove FPKM and "")
#col 7: cov (remove cov and "")
#col 8: full_read_support (remove full_read_support and "")
#finally, sort entire spreadsheet in assending order with col A and then col B
###########

if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: write_expressed_seq_from_cufflinks_gtf_output.py .gtfFILE annotated_transcripts_FILE"
		sys.exit()
		
	infile = open(sys.argv[1],"rU")
	trans_id_dict = {}
	for line in infile:
		if len(line) < 3: continue
		line = line.strip().split('\t')
		trans_id_dict[line[5]] = line[7]

	infile = open(sys.argv[2],"rU")
	outfile = open(sys.argv[2][:-3]+"_expressed.fa","w")
	for record in SeqIO.parse(infile,"fasta"):
		trans_id = (record.id).split("|")[1].replace("pacid:","")
		trans_id = trans_id.replace("PACid:","")
		seq = record.seq
		if trans_id in trans_id_dict:
			outfile.write(">"+trans_id+"_"+trans_id_dict[trans_id]+"\n")
			outfile.write(str(seq)+"\n")
	infile.close()
	outfile.close()

