SOAPdenovo-Trans-127mer pregraph -s soapdenovo-trans_config -p 10 -K 21 -o PAZJ_21
SOAPdenovo-Trans-127mer pregraph -s soapdenovo-trans_config -p 10 -K 31 -o PAZJ_31
SOAPdenovo-Trans-127mer pregraph -s soapdenovo-trans_config -p 10 -K 41 -o PAZJ_41
SOAPdenovo-Trans-127mer pregraph -s soapdenovo-trans_config -p 10 -K 51 -o PAZJ_51
SOAPdenovo-Trans-127mer pregraph -s soapdenovo-trans_config -p 10 -K 61 -o PAZJ_61
SOAPdenovo-Trans-127mer pregraph -s soapdenovo-trans_config -p 10 -K 71 -o PAZJ_71

SOAPdenovo-Trans-127mer contig -g PAZJ_21
SOAPdenovo-Trans-127mer contig -g PAZJ_31
SOAPdenovo-Trans-127mer contig -g PAZJ_41
SOAPdenovo-Trans-127mer contig -g PAZJ_51
SOAPdenovo-Trans-127mer contig -g PAZJ_61
SOAPdenovo-Trans-127mer contig -g PAZJ_71

SOAPdenovo-Trans-31mer map -s soapdenovo-trans_config -g PAZJ_21 -p 10
SOAPdenovo-Trans-127mer map -s soapdenovo-trans_config -g PAZJ_31 -p 10
SOAPdenovo-Trans-127mer map -s soapdenovo-trans_config -g PAZJ_41 -p 10
SOAPdenovo-Trans-127mer map -s soapdenovo-trans_config -g PAZJ_51 -p 10
SOAPdenovo-Trans-127mer map -s soapdenovo-trans_config -g PAZJ_61 -p 10
SOAPdenovo-Trans-127mer map -s soapdenovo-trans_config -g PAZJ_71 -p 10

SOAPdenovo-Trans-31mer scaff -p 10 -g PAZJ_21
SOAPdenovo-Trans-127mer scaff -p 10 -g PAZJ_31
SOAPdenovo-Trans-127mer scaff -p 10 -g PAZJ_41
SOAPdenovo-Trans-127mer scaff -p 10 -g PAZJ_51
SOAPdenovo-Trans-127mer scaff -p 10 -g PAZJ_61
SOAPdenovo-Trans-127mer scaff -p 10 -g PAZJ_71

