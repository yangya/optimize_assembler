"""
process output of detect_chimera_from_blastx.py
write seq with chimera cut to the fastaDIR
"""

import sys,os
from Bio import SeqIO

FASTA_FILE_ENDING = ".fasta"

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python cut_chimera_from_blastx fastaDIR blastxDIR output_len_cutoff"
		sys.exit()
	
	fastaDIR = sys.argv[1]+"/"
	blastxDIR = sys.argv[2]+"/"
	OUTPUT_LEN_CUTOFF = int(sys.argv[3])
	
	for i in os.listdir(fastaDIR):
		if i[-6:] != ".fasta" and i[-3:] != ".fa" and i[-4:] != ".fas":
			continue			
		fastafile = fastaDIR+i
		cutfile = blastxDIR+i+".blastx.cut"
		
		#get info about where to cut
		infile = open(cutfile,"rU")
		cutDICT = {} #key is id, value is [start,end,...]
		for line in infile:
			spls = line.strip().split(" ")
			if len(spls) >= 3:
				name,start,end = spls[0],spls[1],spls[2]
				if name not in cutDICT:
					cutDICT[name] = []
				cutDICT[name].append(int(start))
				cutDICT[name].append(int(end))
		infile.close()
		
		#cut seqs
		print "Cutting",fastafile
		handle = open(fastafile,"rU")
		outfile = open(fastaDIR+i.replace(FASTA_FILE_ENDING,".cutfa"),"w")
		for record in SeqIO.parse(handle,"fasta"):
			seqid,seq = str(record.id),str(record.seq)
			if seqid in cutDICT:
				cutrange = sorted(cutDICT[seqid])
				if len(cutrange) == 2: #only one seq to output
					start,end = cutrange[0],cutrange[1]
					seq = seq[start-1:end]
					if len(seq) >= OUTPUT_LEN_CUTOFF:
						outfile.write(">"+seqid+"\n"+seq+"\n")
				elif len(cutrange) == 4: #may output two seqs
					#sorting removes the overlapping range
					start1,end1 = cutrange[0],cutrange[1]
					start2,end2 = cutrange[2],cutrange[3]
					seq1,seq2 = seq[start1-1:end1],seq[start2-1:end2]
					if len(seq1) >= OUTPUT_LEN_CUTOFF and len(seq2) >= OUTPUT_LEN_CUTOFF:
						outfile.write(">"+seqid+"a\n"+seq1+"\n")
						outfile.write(">"+seqid+"b\n"+seq2+"\n")
					else:
						if len(seq1) >= OUTPUT_LEN_CUTOFF:
							outfile.write(">"+seqid+"\n"+seq1+"\n")
						if len(seq2) >= OUTPUT_LEN_CUTOFF:
							outfile.write(">"+seqid+"\n"+seq2+"\n")
				else:
					print "more than 2 cut ranges for the sequence",seqid
					exit()
			else: #no need to cut
				if len(seq) >= OUTPUT_LEN_CUTOFF:
					outfile.write(">"+seqid+"\n"+seq+"\n")
		handle.close()
		outfile.close()
