"""
Useing the default K=23 since benchmark by Yang and Smith BMC Genomics 2013
shows that soapdenovo-trans has low error rate even at K as low as 21
However, completeness drops as K increases, especially when K > 31

Modify the address of EXECUTABLE_PATH and copy the executable SOAPdenovo-Trans-31mer there
Modify the MAX_

This script will create a working dir, write a config file and carry out the assembly
"""


import os,sys

MAX_RD_LEN = 90 #longest read lengh
AVER_INS = 200	#average insertion size for the library

EXECUTABLE_PATH = "./"

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python soapdenovo-trans_wrapper fq1 fq2 outDIR"
		sys.exit()
	
	fq1 = sys.argv[1]
	fq2 = sys.argv[2]
	fqname = fq1.split(".")[0]
	outDIR = sys.argv[3]
	if outDIR[-1] != "/": outDIR += "/"
	
	#make a working dir
	os.system("mkdir "+outDIR)
	
	#write a config file
	out = "max_rd_len="+str(MAX_RD_LEN)+"\n[LIB]\navg_ins="+str(AVER_INS)+"\n"
	out += "q1="+fq1+"\nq2="+fq2+"\nreverse_seq=0\nasm_flags=3\n"
	with open(outDIR+"config","w") as outfile:
		outfile.write(out)
	
	#run assembly with defalt K=23
	cmd = EXECUTABLE_PATH+"SOAPdenovo-Trans-31mer all -s "+outDIR+"config -o "+outDIR+fqname
	print cmd
	os.system(cmd)
